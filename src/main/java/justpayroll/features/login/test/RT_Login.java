package justpayroll.features.login.test;

import justpayroll.common.CONSTANTS;
import justpayroll.common.JustPayrollAction;
import justpayroll.features.login.LoginCMD;
import justpayroll.features.login.LoginObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

public class RT_Login extends JustPayrollAction {

    public LoginCMD loginLogout;
    public LoginObject object;

    @BeforeSuite
    public void initialize(){
        loginLogout = PageFactory.initElements(driver, LoginCMD.class);
        object = PageFactory.initElements(driver, LoginObject.class);

        // TESTING
        // Testing 2
    }

    @Test(priority = 1, description = "Execute login for super admin account")
    public void loginAdmin() throws InterruptedException, IOException {
        Thread.sleep(1000);
        loginLogout.loginSetup(CONSTANTS.ADMIN);

    }

    @Test(priority = 2, description = "Execute login for worker account")
    public void loginWorker() throws InterruptedException, IOException {
        Thread.sleep(1000);
        loginLogout.loginSetup(CONSTANTS.WORKER);

    }

    @Test(priority = 3, description = "Execute logout account")
    public void logOut() throws InterruptedException {
        Thread.sleep(1000);
        loginLogout.logOut();

    }

}
