package justpayroll.Features.Login;

import justpayroll.common.JustPayrollAction;
import justpayroll.common.CommonMethods;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;


public class LoginCMD extends JustPayrollAction {

    private final CommonMethods  methods = PageFactory.initElements(driver, CommonMethods.class);

    public void loginSetup(String type) throws InterruptedException, IOException {
        methods.setupBrowser();

        Thread.sleep(1000);
        LoginObject object = PageFactory.initElements(driver, LoginObject.class);

        String[] result = methods.loginIfLoggedOut(type,"","");

        object.txtUsername.sendKeys(result[1]);
        object.txtPassword.sendKeys(result[2]);
        object.btnLogin.click();
        Thread.sleep(5000);

    }

    public void logOut(){

        LoginObject object = PageFactory.initElements(driver, LoginObject.class);

        object.btnElipsLogout.click();
        object.btnLogout.click();
    }


}