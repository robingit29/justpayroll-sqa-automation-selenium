package justpayroll.Features.Login;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginObject {
    @FindBy(xpath = "//input[@name=\"username\"]")
    public WebElement txtUsername;

    @FindBy(xpath = "//input[@name=\"password\"]")
    public WebElement txtPassword;

    @FindBy(xpath = "//button[@class=\"md-raised md-primary md-button md-ink-ripple w-100 btn-login-form\"]")
    public WebElement btnLogin;

    @FindBy(xpath = "//i[contains(text(),'more_vert')]")
    public WebElement btnElipsLogout;

    @FindBy(xpath = "/html/body/div[2]/md-menu-content/md-menu-item[2]/button")
    public WebElement btnLogout;


}
