package justpayroll.features.roles.test;

import justpayroll.common.CONSTANTS;
import justpayroll.common.JustPayrollAction;
import justpayroll.common.NavigationAction;
import justpayroll.features.login.LoginCMD;

import justpayroll.features.roles.RolesCMD;
import justpayroll.features.roles.RolesObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import justpayroll.features.login.LoginObject;
import justpayroll.common.NavigationAction;

import java.io.IOException;

public class RT_Roles extends JustPayrollAction {

    private LoginCMD loginLogout;

    private RolesObject object;
    private RolesCMD roles;
    private  NavigationAction navigate;

    @BeforeSuite
    public void initialize(){

        loginLogout = PageFactory.initElements(driver, LoginCMD.class);
        object = PageFactory.initElements(driver, RolesObject.class);
        roles = PageFactory.initElements(driver, RolesCMD.class);
        navigate = PageFactory.initElements(driver, NavigationAction.class);

    }

    @Test(priority = 1, description = "Create Account using Super Admin")
    public void login() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.ADMIN);
        Thread.sleep(1000);

        roles.createUserAccount();

    }

    // for Worker account
    @Test(priority = 2, description = "Navigate worker account")
    public void accessWorkerAccount() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.WORKER);
        Thread.sleep(1000);

    }

    @Test(priority = 3, description = "Navigate side mune for worker account")
    public void accessProfileWorkerAccount() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.WORKER);
        Thread.sleep(1000);

        navigate.navigateSideMenu(CONSTANTS.LEVEL1.WEB_BUNDY);
        roles.accessWorkerAccount();
    }

}
