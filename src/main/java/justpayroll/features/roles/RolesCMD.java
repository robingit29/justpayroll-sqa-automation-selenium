package justpayroll.features.roles;

import justpayroll.common.JustPayrollAction;
import justpayroll.common.NavigationAction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class RolesCMD extends JustPayrollAction {

    private RolesObject object =  PageFactory.initElements(driver, RolesObject .class);
    private NavigationAction navigate = PageFactory.initElements(driver, NavigationAction.class);


    public void createUserAccount() throws InterruptedException {
        Thread.sleep(1000);
        driver.findElement(By.xpath("/html/body/app/ng-outlet/dashboard/navigation/div[1]/ul/li[2]")).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath("//*[@id=\"administrationSub\"]/li[1]")).click();

        driver.findElement(By.xpath("//div[2]/ng-outlet/usermanagement/div[1]/div[1]/div[2]/div/button")).click();


        driver.findElement(By.xpath("//*[@id=\"input_11\"]")).sendKeys("Nikka");
    }

    public void accessWorkerAccount()throws InterruptedException {
        System.out.println("Successfully access profile");
    }

}


