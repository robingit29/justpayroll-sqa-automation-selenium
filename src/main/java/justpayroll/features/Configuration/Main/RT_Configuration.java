package justpayroll.features.Configuration.Main;

import justpayroll.common.CONSTANTS;
import justpayroll.common.JustPayrollAction;
import justpayroll.common.NavigationAction;
import justpayroll.features.login.LoginCMD;
import justpayroll.features.Configuration.ConfigurationCMD;
import justpayroll.features.Configuration.ConfigurationObject;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;


public class RT_Configuration extends JustPayrollAction {
    private LoginCMD loginLogout;
    private ConfigurationCMD costCenter;

    private ConfigurationObject object;
    private ConfigurationCMD roles;
    private NavigationAction navigate;

    @BeforeSuite
    public void initialize(){

        loginLogout = PageFactory.initElements(driver, LoginCMD.class);
        object = PageFactory.initElements(driver, ConfigurationObject.class);
        roles = PageFactory.initElements(driver, ConfigurationCMD.class);
        navigate = PageFactory.initElements(driver, NavigationAction.class);
        costCenter = PageFactory.initElements(driver, ConfigurationCMD.class);

    }

    // Adding script for Creating Cost Center
    @Test(priority = 1, description = "Create Cost Center")
    public void createCostCenter() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.ADMIN);
        Thread.sleep(5000);
        navigate.navigateSideMenu(CONSTANTS.LEVEL1.CONFIGURATION);
        Thread.sleep(1000);
        navigate.setNavigateConfiguration(CONSTANTS.LEVEL2.COST_CENTER);
        Thread.sleep(1000);
        navigate.configurationsBtn(CONSTANTS.Configurations.addCostCenter);
        Thread.sleep(1000);
        costCenter.createCostCenter(CONSTANTS.Configurations.createCostCenter);
        Thread.sleep(1000);

    }

    // Adding script for Creating Cost Center : Robin
    @Test(priority = 2, description = "Create Deminis compensation")
    public void createDeminisCompensation() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.ADMIN);
        Thread.sleep(5000);
        navigate.navigateSideMenu(CONSTANTS.LEVEL1.CONFIGURATION);
        Thread.sleep(2000);
        navigate.navigateSideMenu(CONSTANTS.LEVEL2.COMPENSATION);
        Thread.sleep(2000);
        navigate.navigateSideMenu(CONSTANTS.Configurations.addDeminisDots);
        Thread.sleep(2000);
        navigate.navigateSideMenu(CONSTANTS.Configurations.addDeminis);
        Thread.sleep(2000);
        costCenter.createCostCenter(CONSTANTS.Configurations.createDeminis);

    }

}
