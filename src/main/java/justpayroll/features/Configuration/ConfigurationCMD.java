package justpayroll.features.Configuration;

import justpayroll.common.CommonMethods;
import justpayroll.common.JustPayrollAction;
import justpayroll.features.Configuration.ConfigurationObject;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class ConfigurationCMD extends JustPayrollAction {

    // Adding scripts for functions of creating Cost Center : Jerald
    public void createCostCenter(String type) throws InterruptedException, IOException {

        final CommonMethods methods = PageFactory.initElements(driver, CommonMethods.class);

        Thread.sleep(1000);
        ConfigurationObject object = PageFactory.initElements(driver, ConfigurationObject.class);

        String[] result = methods.costCenter(type,"","");

        object.txtCode.sendKeys(result[1]);
        object.txtName.sendKeys(result[2]);
        object.saveBtn.click();


    }

    public void createCompensation(String type) throws InterruptedException, IOException {

        final CommonMethods methods = PageFactory.initElements(driver, CommonMethods.class);

        Thread.sleep(1000);
        ConfigurationObject object = PageFactory.initElements(driver, ConfigurationObject.class);

        String[] result = methods.deminisComp(type,"","");

        object.txtCode.sendKeys(result[1]);
        object.txtName.sendKeys(result[2]);
        object.saveBtn.click();


    }
}
