package justpayroll.features.Configuration;

import justpayroll.common.JustPayrollAction;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ConfigurationObject extends JustPayrollAction {

    // Adding script for xpath for Add Cost Center : Jerald
    @FindBy(xpath = "/html/body/div[2]/md-dialog/form/md-dialog-content/div[1]/md-input-container/input")
    public WebElement txtCode;

    @FindBy(xpath = "/html/body/div[2]/md-dialog/form/md-dialog-content/div[2]/md-input-container/input")
    public WebElement txtName;

    @FindBy(xpath = "/html/body/div[2]/md-dialog/form/md-dialog-actions/div/button")
    public WebElement saveBtn;




    }



