package justpayroll.Features.Dashboard;

import justpayroll.common.CommonMethods;
import justpayroll.common.JustPayrollAction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import java.util.Random;

public class DashboardObject extends JustPayrollAction{
        private static Random rand = new Random();



        // Adding script for xpath for Create Company : Jerald
//       @FindBy(xpath = "//input[contains(@name,'name')]")
//       public WebElement txtCompanyName;

        // Creating company Information - Jerald
        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[1]/div[1]/md-input-container/input")
        public WebElement txtCompanyName;


//       @FindBy(xpath = "//input[contains(@name,'externalGroupId')]")
//       public WebElement txtCompanyId;

        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[1]/div[2]/md-input-container/input")
        public WebElement txtCompanyId;

        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[2]/div[2]/md-input-container/md-select")
        public WebElement selCompOrgType;

        @FindBy(xpath = "//div[contains(text(),'Head Office')]")
        public WebElement headOfficeType;

//       @FindBy(xpath = "//input[contains(@name,'secRegistrationNumber')]")
//       public WebElement secRegistration;

        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[3]/div[1]/md-input-container/input")
        public WebElement secRegistration;

//       @FindBy(xpath = "//input[contains(@name,'natureOfBusiness')]")
//       public WebElement natureOfBusiness;

        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[3]/div[2]/md-input-container/input")
        public WebElement natureOfBusiness;

        @FindBy(xpath = "/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/company-add/div/div[2]/div/div[1]/form/div[1]/div/div[1]/div[1]/div[2]/div[2]/div[4]/div[1]/md-input-container/md-select")
        public WebElement registrationType;

        @FindBy(xpath = "//div[contains(text(),'Domestic Corporation ')]")
        public WebElement selectedRegistrationType;

        @FindBy(xpath = "//input[contains(@aria-label, 'tin')]")
        public WebElement txtTIN;

        @FindBy(xpath = "//input[contains(@aria-label, 'sss')]")
        public WebElement txtSSS;

        @FindBy(xpath = "//input[contains(@aria-label, 'philHealthNumber')]")
        public WebElement txtPhHeath;

        @FindBy(xpath = "//input[contains(@aria-label, 'pagIbigNumber')]")
        public WebElement txtPagibig;

        @FindBy(xpath = "//input[contains(@aria-label, 'signatoryName')]")
        public WebElement txtSigName;

        @FindBy(xpath = "//input[contains(@aria-label,'signatoryDesignation')]")
        public WebElement txtSigPosition;


        // Creating Contact Details of Company
        @FindBy(xpath = "//input[contains(@aria-label,'address')]")
        public WebElement txtAddress;

        @FindBy(xpath = "//md-select[contains(@aria-label,'workCity')]")
        public WebElement txtCity;



        @FindBy(xpath = "//input[contains(@aria-label,'zip')]")
        public WebElement txtZIP;

        @FindBy(xpath = "//input[contains(@aria-label,'rdo')]")
        public WebElement txtRDO;

        @FindBy(xpath = "//input[contains(@aria-label,'email')]")
        public WebElement txtEmail;

        @FindBy(xpath = "//input[contains(@aria-label,'phone')]")
        public WebElement txtPhone;

        @FindBy(xpath = "//input[contains(@aria-label,'mobile')]")
        public WebElement txtMobile;

        public static String randomValue(){

                String[] value = {"ANTIPOLO","BAGUIO"};

                return value[rand.nextInt(value.length)];
        }


        public WebElement ele = driver.findElement(By.xpath("//md-option[contains(@value,'"+randomValue()+"')]"));












}

