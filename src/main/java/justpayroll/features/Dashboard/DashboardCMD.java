package justpayroll.Features.Dashboard;

import justpayroll.Features.Dashboard.DashboardObject;
import justpayroll.Features.Login.LoginCMD;
import justpayroll.common.CommonMethods;
import justpayroll.common.JustPayrollAction;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;


public class DashboardCMD extends JustPayrollAction {


    public void createCompanyInfo(String type) throws InterruptedException {

        final CommonMethods methods = PageFactory.initElements(driver, CommonMethods.class);

        Thread.sleep(1000);
        DashboardObject object = PageFactory.initElements(driver, DashboardObject.class);

        String[] result = methods.createCompanyInfo(type,"","","","","","","");

        object.txtCompanyName.sendKeys(result[1]);
        object.txtCompanyId.sendKeys(result[2]);

        object.selCompOrgType.click();
        object.headOfficeType.click();

        object.secRegistration.sendKeys(result[5]);
        object.natureOfBusiness.sendKeys(result[4]);

        object.registrationType.click();
        object.selectedRegistrationType.click();

        object.txtTIN.sendKeys(result[3]);
        object.txtSSS.sendKeys(result[3]);
        object.txtPhHeath.sendKeys(result[3]);
        object.txtPagibig.sendKeys(result[3]);
        object.txtSigName.sendKeys(result[6]);
        object.txtSigPosition.sendKeys(result[7]);

//        object.txtCity.click();
//        object.ele.click();






    }


}

