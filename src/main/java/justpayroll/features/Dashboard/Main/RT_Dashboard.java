package justpayroll.Features.Dashboard.Main;


import justpayroll.Features.Dashboard.DashboardObject;
import justpayroll.Features.Dashboard.DashboardCMD;
import justpayroll.Features.Login.LoginCMD;
import justpayroll.common.CONSTANTS;
import justpayroll.common.JustPayrollAction;
import justpayroll.common.NavigationAction;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

public class RT_Dashboard extends JustPayrollAction {
    private LoginCMD loginLogout;
    private NavigationAction navigate;
    private DashboardCMD Company;

    @BeforeSuite
    public void initialize(){

        loginLogout = PageFactory.initElements(driver, LoginCMD.class);
        navigate = PageFactory.initElements(driver, NavigationAction.class);
        Company = PageFactory.initElements(driver, DashboardCMD.class);

    }

    // Adding script for Creating Company
    @Test(priority = 1, description = "Creating Company Information")
    public void createCompanyInfo() throws InterruptedException, IOException {
        loginLogout.loginSetup(CONSTANTS.SUPER_ADMIN);
        navigate.navigateSideMenu(CONSTANTS.LEVEL1.DASHBOARD);
        navigate.setNavigateDashboard(CONSTANTS.LEVEL2.CREATE_COMPANY);
        Thread.sleep(2000);
        navigate.navigateDashboardBtn(CONSTANTS.Company.AddCompanyBtn);
        Thread.sleep(2000);
        Company.createCompanyInfo(CONSTANTS.Dahsboard.createCompany);

    }
//    @Test(priority = 2, description = "Creating Contact Details")
//    public void createContacts() throws InterruptedException, IOException {
//
//
//    }
//    @Test(priority = 3, description = "Creating Bank Information")
//    public void createBankInfo() throws InterruptedException, IOException {
//
//
//    }
}
