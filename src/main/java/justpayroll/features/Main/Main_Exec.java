package justpayroll.Features.Main;

import justpayroll.Features.Dashboard.DashboardCMD;
import justpayroll.Features.Login.LoginCMD;
import justpayroll.Features.Login.LoginObject;
import justpayroll.common.CONSTANTS;
import justpayroll.common.CommonMethods;
import justpayroll.common.JustPayrollAction;
import justpayroll.common.NavigationAction;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.io.IOException;

public class Main_Exec extends JustPayrollAction {

    public LoginCMD loginLogout;
    public LoginObject object;
    private NavigationAction navigate;
    private DashboardCMD Company;

    @BeforeSuite
    public void initialize(){


        loginLogout = PageFactory.initElements(driver, LoginCMD.class);
        object = PageFactory.initElements(driver, LoginObject.class);
        navigate = PageFactory.initElements(driver, NavigationAction.class);
        Company = PageFactory.initElements(driver, DashboardCMD.class);

    }

    private final CommonMethods methods = PageFactory.initElements(driver, CommonMethods.class);

    @Test(priority = 1, description = "Execute login for input invalid")
    public void loginInvalidUsername() throws InterruptedException, IOException {
        methods.setupServer();

        Thread.sleep(1000);
        loginLogout.loginSetup(CONSTANTS.INVALID_USERNAME);
        loginLogout.loginSetup(CONSTANTS.INVALID_PASSWORD);
        loginLogout.loginSetup(CONSTANTS.SUPER_ADMIN);

    }


    @Test(priority = 4, description = "Creating Company Information")
    public void createCompanyInfo() throws InterruptedException, IOException {

        navigate.navigateSideMenu(CONSTANTS.LEVEL1.DASHBOARD);
        navigate.setNavigateDashboard(CONSTANTS.LEVEL2.CREATE_COMPANY);
        Thread.sleep(2000);
        navigate.navigateDashboardBtn(CONSTANTS.Company.AddCompanyBtn);
        Thread.sleep(2000);
        Company.createCompanyInfo(CONSTANTS.Dahsboard.createCompany);

    }

//    @Test(priority = 5, description = "Execute logout account")
//    public void logOut() throws InterruptedException {
//        Thread.sleep(1000);
//        loginLogout.logOut();
//
//    }
}
