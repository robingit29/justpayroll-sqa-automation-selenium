package justpayroll.common;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

public class CommonMethods extends JustPayrollAction {
    private GeneralObject  general = PageFactory.initElements(driver, GeneralObject.class);

    public void setupBrowser() throws IOException{
        InputStream input = new FileInputStream(configPath);

        userConfigs.load(input);
        String driverPath = new File(userConfigs.getProperty("DRIVER_PATH")).getAbsolutePath();

        System.setProperty(chromeDriver, driverPath + "\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();


    }

    public void setupServer() throws IOException{
        String getSERVER = userConfigs.getProperty("SERVER").toLowerCase();

        switch (getSERVER) {
//          case "upgl": driver.get("https://" + getSERVER + ".justpayroll.ph"); break;
            case "worker": driver.get("https://" + getSERVER + ".philpay.ph/employee/login"); break;
            default:  driver.get("https://" + getSERVER + ".philpay.ph/admin/login"); break;
        }
    }

    // For Login Module
    public String[] loginIfLoggedOut(String type, String getUsername, String getPassword ) {

        if (type == "WORKER") {
            System.out.println("worker");
            getUsername = userConfigs.getProperty("WORKER_USERNAME");
            getPassword = userConfigs.getProperty("DEFAULT_PASSWORD");
        } else if (type == "ADMIN") {
            System.out.println("admin");
            getUsername = userConfigs.getProperty("ADMIN_USERNAME");
            getPassword = userConfigs.getProperty("ADMIN_PASSWORD");
        } else  if (type == "SUPER_ADMIN"){
            System.out.println("super admin");
            getUsername = userConfigs.getProperty("SUPER_ADMIN_USERNAME");
            getPassword = userConfigs.getProperty("SUPER_ADMIN_PASSWORD");
        }else  if (type == "INVALID_USERNAME"){
            System.out.println("super admin");
            getUsername = userConfigs.getProperty("INVALID_USERNAME");
            getPassword = userConfigs.getProperty("VALID_PASSWORD");
        }else  if (type == "INVALID_PASSWORD"){
            System.out.println("super admin");
            getUsername = userConfigs.getProperty("VALID_USERNAME");
            getPassword = userConfigs.getProperty("INVALID_PASSWORD");
        }

        return new String[]{type, getUsername, getPassword};

    }

    // Adding script for acquiring data from RandomObject class for Cost Center
    public String[] costCenter(String type, String getCode, String getName) {

        if (type == "Create Cost Center") {
            getCode = RandomObject.randomizer().toUpperCase();
            getName = RandomObject.randomizer().toUpperCase();
        }else System.out.println("Undefied");

        return new String[]{type, getCode, getName};

    }

    // Adding script for acquiring data from RandomObject class for Department
    public String[] department(String type, String getCode, String getName) {

        if (type == "Create Department") {
            getCode = RandomObject.randomizer().toUpperCase();
            getName = RandomObject.randomVal().toUpperCase();
        }else System.out.println("Undefied");

        return new String[]{type, getCode, getName};

    }

    // Adding script for acquiring data from RandomObject class for Compensation
    public String[] deminisComp(String type, String getCode, String getName) {

        if (type == "Create Deminis") {
            getCode = RandomObject.randomizer().toUpperCase();
            getName = RandomObject.randomizer().toUpperCase();
        }else System.out.println("Undefied");

        return new String[]{type, getCode, getName};

    }

    // Adding script for acquiring data from RandomObject class for Creating Company Information
    public String[] createCompanyInfo(String type, String getCompanyId, String getCompanyName, String getNum, String getNumNOB, String getSecNum, String getSigName, String getSigPos) {

        if (type == "Create Company") {
            getCompanyName = RandomObject.randomizer().toUpperCase();
            getCompanyId = RandomObject.randomizer().toUpperCase();
            getNum = RandomObject.randomNum();
            getNumNOB = RandomObject.randomNOB().toUpperCase();
            getSecNum = RandomObject.randomSecNum().toUpperCase();
            getSigName = RandomObject.randomSignatoryName().toUpperCase();
            getSigPos = RandomObject.randomSignatoryPosition();


        }else System.out.println("Undefied");

        return new String[]{type, getCompanyId, getCompanyName, getNum, getNumNOB, getSecNum, getSigName, getSigPos};

    }

//    public String[] createContact(String type, String)





}
