package justpayroll.common;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;

public class RandomObject {
    private static Random rand = new Random();

    // Random values for Cost Center

    public static String nameCc() {
        String[] name = {"TESTING11","testing12","testing13","testing14","testing15"};

        return name[rand.nextInt(name.length)];
        }

    public static String codeCc() {
        String[] code = {"CC0009","CC0010","CC0011","CC0012","CC0013"};

        return code[rand.nextInt(code.length)];
    }

    // Random values for Compensation Deminis

    public static String nameDmn() {
        String[] name = {"TESTING11","testing12","testing13","testing14","testing15"};

        return name[rand.nextInt(name.length)];
    }

    public static String codeDmn() {
        String[] code = {"CC0009","CC0010","CC0011","CC0012","CC0013"};

        return code[rand.nextInt(code.length)];
    }

}




