package justpayroll.common;

public class CONSTANTS {
    /*
     *
     * REMINDERS
     * 1. USE UPPERCASE IN NAMING CONSTANT VARIABLES FOR EASIER SEARCHING
     * 2. SEPARATE MULTIPLE WORDS WITH UNDERSCORE _
     * 3. FOR COMMONLY USED TERMS/LABELS, PUT UNDER CONSTANTS
     *
     *
     */

    public static final String ADMIN = "ADMIN";
    public static final String WORKER = "WORKER";
    public static final String SUPER_ADMIN = "SUPER_ADMIN";
    public static final String INVALID_USERNAME = "INVALID_USERNAME";
    public static final String INVALID_PASSWORD = "INVALID_PASSWORD";


    public class SERVERS{
        private SERVERS(){}

        //Type all server
        public static  final String SIT = "SIT";
        public static  final String UPGL = "UPGL";


    }

    public final class LABELS{
        private LABELS(){}
        //Type all the possible labels needed
        public static final String TENANT = "Tenant";


    }

    public final class LEVEL1{
        private LEVEL1() {}

        // Type all the main modules which located on the 1st part of sidemenu
        public static final String DASHBOARD = "Dashboard";

        // For worker account
        public static final String PROFILE = "Profile";
        public static final String ATTENDANCE = "Attendance";
        public static final String WEB_BUNDY = "Web Bundy";

        // Add Script for navigation to Configuration Module : Jerald
        public static final String CONFIGURATION = "Configurations";
        public static final String COMPANY = "Company";

        public static final String REQUEST_CATEGORY = "Request Category";
        public static  final String PAYSLIPS = "Payslips";
        public static final String REPORTS = "Reports";
        public static final String ABOUT = "About";

        // Add script for declaration of variable for creating Cost Center : Jerald
        public static final String COST_CENTER = "Cost Center";
        public static final String ADD = "Add";

    }

    public final class LEVEL2{
        private LEVEL2(){}
        public static final String CREATE_COMPANY = "Create Company";

        public static final String COMPENSATION = "Compensation";
        public static final String LEAVES = "Leaves";
        public static final String COST_CENTER = "Cost Center";
        public static final String DEPARTMENT = "Department";


        // Type all the main sub-modules which located under 1st part of sidemenu

    }

    public final class Configurations{
        public static final String addCostCenterBtn = "Add Cost Center";

        public static final String addDeminisDotsBtn = "Navigate Add Deminis Button";
        public static final String addDeminisBtn = "Add Deminis";

        public static final String createCostCenter = "Create Cost Center";
        public static final String createDeminisBtn = "Create Deminis";

        public static final String addDepartmentBtn = "Add Department";
        public static final String createDepartment = "Create Department";


    }

    public final class Company{
        public static final String AddCompanyBtn = "Add Company";
    }

    public final class Dahsboard{
        public static final String createCompany = "Create Company";
    }

    // Add another class Level if needed..



}
