package justpayroll.common;

import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class NavigationAction  extends JustPayrollAction{

    /*
     *
     * REMINDERS
     * 1. Contains all methods when navigating side menu
     *
     *
     */

    private CommonMethods methods = PageFactory.initElements(driver, CommonMethods.class);

    public void navigateSideMenu(String navigateSideMenuText) {
        switch (navigateSideMenuText) {

            case CONSTANTS.LEVEL1.DASHBOARD:
                driver.findElement(By.xpath("//ul/li[1]/a/i")).click();
                break;
            case CONSTANTS.LEVEL1.COMPANY:
                driver.findElement(By.xpath("//ul/li[3]/a/i")).click();
                break;
            case CONSTANTS.LEVEL1.CONFIGURATION:
                driver.findElement(By.xpath("//ul/li[4]/a/i")).click();
                break;

        }
    }
    // Xpath Element for Configuration Sub Menus
    public void setNavigateDashboard(String navigateSubMenuText) {
        switch (navigateSubMenuText) {

            case CONSTANTS.LEVEL2.CREATE_COMPANY:
                driver.findElement(By.xpath("//i[contains(text(), 'menu')]")).click();
                break;



        }
    }

    public void navigateDashboardBtn(String navigateSubMenuText) {
        switch (navigateSubMenuText) {

            case CONSTANTS.Company.AddCompanyBtn:
                driver.findElement(By.xpath("//i[contains(text(), 'add')]")).click();
                break;

        }
    }
    // Xpath Element for Configuration Sub Menus
    public void setNavigateConfiguration(String navigateSubMenuText){
        switch (navigateSubMenuText){

            case CONSTANTS.LEVEL2.COMPENSATION:
                driver.findElement(By.xpath("//a[contains(text(), 'Compensations')]")).click();
                break;
            case CONSTANTS.LEVEL2.LEAVES:
                driver.findElement(By.xpath("//a[contains(text(), 'Leaves')]")).click();
                break;
            case CONSTANTS.LEVEL2.COST_CENTER:
                driver.findElement(By.xpath("//a[contains(text(), 'Cost Centers')]")).click();
                break;
            case CONSTANTS.LEVEL2.DEPARTMENT:
                driver.findElement(By.xpath("//a[contains(text(), 'Departments')]")).click();
                break;

        }
    }
    public void configurationsBtn(String navigateSubMenuText){
        switch (navigateSubMenuText){
            case CONSTANTS.Configurations.addCostCenterBtn:
                driver.findElement(By.xpath("//ng-outlet/costcenter/div[1]/div[2]/button[1]")).click();
                break;
            case CONSTANTS.Configurations.addDeminisDotsBtn:
                driver.findElement(By.xpath("//button/i[@class='material-icons ng-binding ng-scope']")).click();
                break;

            case CONSTANTS.Configurations.addDeminisBtn:
                driver.findElement(By.xpath("//button[contains(text(),'Add De Minimis')")).click();
                break;
            case CONSTANTS.Configurations.addDepartmentBtn:
                driver.findElement(By.xpath("//button[contains(text(), 'ADD')]")).click();
                break;


        }
    }

//    public void navigateDepartmentbtn(String navigateSubMenuText){
//        switch (navigateSubMenuText){
//            case CONSTANTS.Configurations.ADD:
//                driver.findElement(By.xpath("/html/body/app/ng-outlet/dashboard/div[1]/div/div[2]/ng-outlet/department/div[1]/div[2]/button[1]")).click();
//                break;
//
//        }
//    }



}
